import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList, AngularFireAction, DatabaseSnapshot,  DatabaseReference } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AngularFireStorage, AngularFireStorageReference } from 'angularfire2/storage';
import * as firebase from 'firebase';
@Injectable()
export class DataService {
  phone_list;
  equal_to_brand;
  phone ;
  constructor(private db: AngularFireDatabase, private storage: AngularFireStorage) {
    this.phone_list = db.list<any>('/');

   }


   getListPhone(){
      return this.phone_list = this.db.list<any>('/').valueChanges();
   }

   getPhone(id: string){
     this.phone = this.db.list('/', (query: DatabaseReference) =>  {
       return query.orderByChild('id').equalTo(parseInt(id))
     }).valueChanges();

     return this.phone;
   }


   queryOrderByDate(){
     const ordered_list = this.db.list('/', (query: DatabaseReference) => {
       return query.orderByChild('releaseDate');
     })
     .valueChanges();

     return ordered_list;
   }

   queryEqualTo(brand : string){
     this.equal_to_brand = this.db.list('/', (query: DatabaseReference) => {
        return query.orderByChild('brand').equalTo(brand);
      })
      .valueChanges();
     if(brand == ' '){
        this.equal_to_brand = this.db.list('/').valueChanges();
     }

     return this.equal_to_brand;
   }


}
