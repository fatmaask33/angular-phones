import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
declare var firebase: any;
@Component({
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [DataService]
})
export class ProductComponent implements OnInit {

  phone ;
  productID: any;

  constructor(private dataService: DataService, private route:ActivatedRoute) {
    this.productID = route.snapshot.params['id'];

  }


  ngOnInit() {
    this.phone = this.dataService.getPhone(this.productID);
    console.log(this.phone);
    /*firebase.database().ref('/'+this.productID).on('child_added', ( snapshot) => {
      this.product[snapshot.key] = snapshot.val();

    })*/


  }



}
