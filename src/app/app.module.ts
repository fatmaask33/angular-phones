import { BrowserModule , Title} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product.component';
import  { SmartphonesComponent } from './smartphones/smartphones.component';
import { AboutComponent } from './about/about.component';
import { WhatsnewComponent } from './whatsnew/whatsnew.component';
import { AngularFireModule } from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database'
import { firebaseConfig } from '../environments/firebase.config';
import { DataService } from './data.service';
import { AngularFireStorageModule } from 'angularfire2/storage';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'smartphones', component: SmartphonesComponent },
  { path: 'product/:id', component: ProductComponent },
  { path: 'whatsnew', component: WhatsnewComponent },
  { path: 'about', component: AboutComponent },
  { path: 'home', redirectTo: '/', pathMatch: 'full' },
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
     AngularFireStorageModule,
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ProductComponent,
    SmartphonesComponent,
    AboutComponent,
    WhatsnewComponent,
  ],
  providers:[Title, DataService],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
