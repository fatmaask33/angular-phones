import { Component, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { DataService } from '../data.service';
@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ Title],
})
export class HomeComponent implements OnInit {
  products;
  constructor( private titleService: Title, private dataService: DataService) { }


  ngOnInit() {
    this.products = this.dataService.getListPhone();
    console.log(this.products);

  }

  getBrand(brand: string){
    this.products =  this.dataService.queryEqualTo(brand);
    console.log(this.products);
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
}
