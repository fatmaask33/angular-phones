import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Title }     from '@angular/platform-browser';
@Component({
  templateUrl: './smartphones.component.html',
  styleUrls: ['../home/home.component.css', './smartphones.component.css'],
  providers: [ Title],
})
export class SmartphonesComponent implements OnInit {
  products;

  constructor( private titleService: Title, private dataService: DataService) { }


  ngOnInit() {
    this.products = this.dataService.getListPhone();
    console.log(this.products);
  }


  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
}
